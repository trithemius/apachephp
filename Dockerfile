#FROM ubuntu:xenial
FROM ubuntu

MAINTAINER Trithemius <maccabeo@gmail.com>

RUN apt-get update && apt-get -y install apache2 locales php7.0-gd php libapache2-mod-php7.0 libapache2-mod-php php-mysql php-mbstring curl wget

RUN apt -y autoremove

#RUN mkdir /www
#RUN mkdir /wwwlog
#RUN chown -R www-data:www-data /www
#RUN chown -R www-data:www-data /wwwlog
#RUN rm -fr /etc/apache2
#RUN mkdir /etc/apache2

#COPY apache2/apache2.conf /etc/apache2
#COPY apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled

COPY info.php /var/www/html
#COPY bash.bashrc /etc/

RUN a2enmod rewrite 
RUN /usr/sbin/phpenmod mysqli

#COPY entrypoint.sh /

#RUN /bin/bash

#ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 80                                                                                                   
#WORKDIR /www                                                                                                   
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]  
